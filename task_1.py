# Створіть клас, який описує книгу. Він повинен містити інформацію про автора,
# назву, рік видання та жанрі. Створіть кілька книжок. Визначте для нього
# операції перевірки на рівність та нерівність, методи __repr__ та __str__.


class Genre:
    def __init__(self, name, description):
        self.name = name
        self.description = description

    def __repr__(self):
        return f"Genre({self.name}, {self.description})"

    def __str__(self):
        return self.name


class Author:
    def __init__(self, first_name, last_name, birth_date):
        self.first_name = first_name
        self.last_name = last_name
        self.birth_date = birth_date

    def __repr__(self):
        return f"Author({self.first_name}, {self.last_name}, {self.birth_date})"

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class Book:
    def __init__(self, authors: list[Author], title: str, year, genre: Genre):
        self.authors = authors
        self.title = title
        self.year = year
        self.genre = genre

    def __repr__(self):
        return f"Book({self.authors}, {self.title}, {self.year}, {self.genre})"

    def __str__(self):
        return f"{self.title} by {', '.join([str(author) for author in self.authors])} ({self.year})"

    def __eq__(self, other):
        return self.title == other.title and set(self.authors) == set(other.authors)

    def __ne__(self, other):
        return not self.__eq__(other)


if __name__ == "__main__":
    fantasy = Genre("Fantasy", "A genre of speculative fiction set in a fictional universe, often inspired by real world myth and folklore.")
    author = Author("J. R. R.", "Tolkien", "1892-01-03")
    author2 = Author("George R. R.", "Martin", "1948-09-20")

    book = Book([author], "The Lord of the Rings", 1954, fantasy)
    book2 = Book([author2], "A Game of Thrones", 1996, fantasy)
    book3 = Book([author, author2], "The Hobbit", 1937, fantasy)
    book4 = Book([author2, author], "The Hobbit", 1937, fantasy)

    print(book3 == book4) #  book3.__eq__(book4)
    print(book3 == book2)
    print(book3 != book4) #  book3.__nq__(book4)
    print(book3 != book2)

